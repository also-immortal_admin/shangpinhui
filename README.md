<!--
 *  ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 *  │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 *  └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 *  ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 *  │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│） 0│_ -│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 *  ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 *  │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{ [│} ]│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 *  ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 *  │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 *  ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 *  │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 *  ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 *  │ Ctrl│    │Alt │         Space         │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 *  └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 *
 * @Descripttion:
 * @Version: 1.0
 * @Author: ltf
 * @Date: 2024-01-02 16:21:30
 * @LastEditors: Do not edit
 * @LastEditTime: 2024-01-24 17:29:21
 -->

# 尚品汇

## 一. vue-cli 脚手架初始化项目。

node + webpack + 淘宝镜像

1. node_modules 文件夹:项目依赖文件夹
2. public 文件夹:一般放置一些静态资源（图片），需要注意，放在 public 文件夹中的静态资源，webpack 进行打包的时候，会原封不动打包到 dist 文件夹中。
3. src 文件夹（程序员源代码文件夹）:  
    (1).assets 文件夹:一般也是放置静态资源（一般放置多个组件共用的静态资源），需要注意，放置在 assets 文件夹里面静态资源，在 webpack 打包的时候，webpack 会把静态资源当做一个模块，打包 JS 文件里面。  
   (2).components 文件夹:一般放置的是非路由组件（全局组件）  
   (3).App.vue:唯一的根组件，vue 当中的组件（.vue）  
   (4).main.js:程序入口文件，也是整个程序当中最先执行的文件
4. babel.config.js:配置文件（babel 相关）
5. package.json 文件:认为项目'身份证'，记录项目叫做什么、项目当中有哪些依赖、项目怎么运行。
6. package-lock.json:缓存性文件
7. readme.md:说明性文件

# 二. 项目的其他配置

1. 项目运行后，自动打开浏览器
   在 package.json 中
   "scripts": {
   &emsp;"serve": "vue-cli-service serve --open",
   &emsp;"build": "vue-cli-service build",
   &emsp;"lint": "vue-cli-service lint"
   },
2. eslint 代码检验功能关闭
   在 vue.config.js 中
   module.exports = defineConfig({
   &emsp;//关闭 eslint
   &emsp; lintOnSave:false,
   })
3. src 配置别名‘@’
   在 jsconfig.json 中
   {
   &emsp;"compilerOptions": {
   &emsp; &emsp; "target": "es5",
   &emsp;&emsp;"module": "esnext",
   &emsp;&emsp; "baseUrl": "./",
   &emsp; &emsp; "moduleResolution": "node",
   &emsp;&emsp; "paths": {
   &emsp;&emsp;&emsp; "@/_": ["src/_"]
   &emsp;&emsp; },
   &emsp; "lib": [
   &emsp;&emsp; "esnext",
   &emsp;&emsp; "dom",
   &emsp;&emsp; "dom.iterable",
   &emsp;&emsp; "scripthost"
   &emsp; &emsp; ]
   &emsp; }
   }

## 三. 项目路由分析

vue-router
前端所谓路由: K-V 键值对
key: URL (地址栏中的路径)
value:相应的路由组件

1. 分析项目结构布局
   路由组件：类似 home，search，login
   非路由组件：Header，Container，Footer
